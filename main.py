import json
import requests


def get_internal_id(prodName):
    # query to rest search API to get the internal ID of a product through its name
    cmd = "https://sobloo.eu/api/v1/services/search?f=identification.externalId:eq:" + prodName + "&include=previews,identification&pretty=true"
    # this API does not need authentication
    res = requests.get(cmd, verify=False)

    if(res.status_code != requests.codes.ok):
        print("Error: %s  for the request cmd <%s>." % (res.status_code, cmd))
        internalId = -1
    else:
        # the returned json object contains the internal Id (only one product returned)
        # print(r.json())
        j = res.json()
    if j["totalnb"] == 0:
        print("No matching products found")
        internalId = -1
    else:
        internalId = j['hits'][0]['data']['uid']
        return internalId


def get_API_key(file_path):
    with open(file_path, 'r') as f:
        key = f.readline().strip()

    return key


def call_API(cmd, auth):
    if auth:
        # Inject secret API key
        headers = {"Authorization": "Apikey {}".format(get_API_key('api.txt'))}
        res = requests.get(cmd, headers=headers, verify=False)
    else:
        res = requests.get(cmd, verify=False)

    if res.status_code != requests.codes.ok:
        print('Error: {} for the request cmd <{}>!'.format(res.status_code, cmd))
    else:
        return res


def wfs_query():
    cmd = "https://sobloo.eu/api/v1/services/wfs?service=WFS&version=3.0.0&request=GetCapabilities"
    res = call_API(cmd, False)

    print(res.content)


def list_sentinel_products(family):
    cmd = "https://sobloo.eu/api/v1/services/search?f=acquisition.missionName:eq:{}".format(family)
    res = call_API(cmd, False)

    print(res.content)


def oneProductMeta(prodName):
    # query to get one product from its name
    cmd = "https://sobloo.eu/api/v1/services/search?f=identification.externalId:eq:" + prodName + "&include=previews,identification&pretty=true"
    res = call_API(cmd, False)

    data = res.json()
    print("Nb product(s): %s" % (data["totalnb"]))
    # Check that there is only one product
    if(data["totalnb"] != 1):
        print("Error, there are more than one product to display")
    else:
        with open("metadata.json", "w") as f:
            f.write(json.dumps(data, indent=4, sort_keys=True))


def get_quick_look(internal_id):
    cmd = "https://sobloo.eu/api/v1/services/thumbnail/{}".format(internal_id)
    res = call_API(cmd, False)

    filename = "thumbnail_{}.png".format("sentinel_2A")
    with open(filename, 'w+b') as f:
        f.write(res.content)


if __name__ == '__main__':
    sentinel_2A = 'S2A_MSIL1C_20180405T235621_N0206_R116_T01WDV_20180406T014524'

    oneProductMeta(sentinel_2A)

    internal_id = get_internal_id(sentinel_2A)
    get_quick_look(internal_id)
