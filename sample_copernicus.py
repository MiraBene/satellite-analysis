import numpy as np

layers = list(wms.contents)

WEST = 732286.77 #/ 4
NORTH = 5578498.16 #/ 4
EAST = 853707.95 #/ 4
SOUTH = 5385275.84 #/ 4

w_ratio = 4
h_ratio = 12

tmp_west = WEST
tmp_south = SOUTH

final_map = np.empty((300*12, 0, 3))

for i_w in range(1, w_ratio+1):
    final_col = np.empty((0, 565, 3))
    
    for i_h in range(1, h_ratio+1):
        tmp_east = get_interpol(WEST, EAST, w_ratio, i_w)
        tmp_north = get_interpol(SOUTH, NORTH, h_ratio, i_h)

        # print(tmp_west, tmp_south, tmp_east, tmp_north, i_w, i_h)
        
        _projection_ = 'EPSG:3857'
        _bbox_ = (tmp_west, tmp_south, tmp_east, tmp_north)
        _time_ = '2018-04-21/2018-04-24'

        _height_ = 300
        _width_ = width(_bbox_, _height_)
        
        layer = wms[layers[0]].name
    
        img = wms.getmap(
            layers=[layer],
            srs = _projection_,
            bbox = _bbox_,
            size = (_width_, _height_),
            format ='image/tiff',
            #time = _time_,
            showlogo = False,
            transparent=False
        )
        
        pil_img = Image.open(img)
        part_npy = np.array(pil_img)

        final_col = np.concatenate((part_npy, final_col), axis=0)
        
        #final_col.append(part_npy)
        
        tmp_south = tmp_north

    tmp_west = tmp_east
    tmp_south = SOUTH
    
    final_map = np.concatenate((final_map, final_col), axis=1)
    plt.imshow(final_map.astype(int))
    plt.show()
    
print(final_map.shape)

W, S, E, N = [146453.3462,5397218.5672,176703.3001,5412429.5358]

#print("WIDTH Toulouse: ", E-W)
#print("WIDTH A-M: ", EAST-WEST)
#print("A-M / Toulouse", (EAST-WEST)/(E-W))
#print('------')
#print("HEIGTH Toulouse: ", N-S)
#print("HEIGTH A-M: ", NORTH-SOUTH)
#print("A-M / Toulouse", (NORTH-SOUTH)/(N-S))